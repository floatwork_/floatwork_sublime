# floatwork_sublime

Meta package to install recommended Sublime Text packages and provide `floatwork_template` auto-completions

## Installation

### Package Control

Launch the *Package Control: Add Respository* command from the command palette to add the floatwork package respository.

`https://bitbucket.org/floatwork_/floatwork_sublime/raw/master/repository.json`

You can now install the package *floatwork* with Package Control as you usually would.

### Git clone

```sh
# Change to your Sublime Text packages directory
cd ~/Library/Application\ Support/Sublime\ Text\ 3/Packages

# Clone this repository as floatwork
git clone https://bitbucket.org/floatwork_/floatwork_sublime floatwork
```

## Usage

This package lets easily you install multiple packages recommended by the floatwork_ team

**Recommended packages:**

- [ApacheConf](https://github.com/colinta/ApacheConf.tmLanguage)
- [CSS To SASS And Stylus Converter](https://github.com/lnikell/css-converter)
- [Babel](https://github.com/babel/babel-sublime)
- [Color Highlight](https://github.com/Kronuz/ColorHighlight)
- [Drunken PHP](https://github.com/idleberg/sublime-drunken-php)
- [EditorConfig](https://github.com/sindresorhus/editorconfig-sublime)
- [Emmet](https://github.com/sergeche/emmet-sublime)
- [Minify](https://github.com/tssajo/Minify)
- [Sass](https://github.com/P233/Syntax-highlighting-for-Sass)
- [SideBarEnhancements](https://github.com/SideBarEnhancements-org/SideBarEnhancements)
- [SublimeLinter](https://github.com/SublimeLinter/SublimeLinter)
- [SublimeLinter-php](https://github.com/SublimeLinter/SublimeLinter-php)
- [Word​Press Completions](https://github.com/kallookoo/sublime-text-wordpress)

To install recommended packages, launch the *floatwork: Install Packages* command. To skip the installation of certain packages, add them to the `ignored_packages` setting:

```json
{
    "ignored_packages": [
        "Minify"
    ]
}
```
