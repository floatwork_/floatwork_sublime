import os, sublime, sublime_plugin, subprocess

package_name = 'floatwork_sublime'

packages = [
    ("ApacheConf", "https://github.com/colinta/ApacheConf.tmLanguage", True),
    ("CSS To SASS And Stylus Converter", "https://github.com/lnikell/css-converter", True),
    ("Babel", "https://github.com/babel/babel-sublime", True),
    ("Color Highlight", "https://github.com/Kronuz/ColorHighlight", True),
    ("DotEnv", "https://github.com/zaynali53/DotENV", True),
    ("Drunken PHP", "https://github.com/idleberg/sublime-drunken-php", True),
    ("EditorConfig", "https://github.com/sindresorhus/editorconfig-sublime", True),
    ("Emmet", "https://github.com/sergeche/emmet-sublime", True),
    ("Gettext", "https://github.com/idleberg/sublime-gettext", True),
    ("Minify", "https://github.com/tssajo/Minify", True),
    ("nginx", "https://github.com/brandonwamboldt/sublime-nginx", True),
    ("Sass", "https://github.com/P233/Syntax-highlighting-for-Sass", True),
    ("SideBarEnhancements", "https://github.com/SideBarEnhancements-org/SideBarEnhancements", True),
    ("SublimeLinter", "https://github.com/SublimeLinter/SublimeLinter", True),
    ("SublimeLinter-php", "https://github.com/SublimeLinter/SublimeLinter-php", True),
    ("Word​Press Completions", "https://github.com/kallookoo/sublime-text-wordpress", True),
    ("Word​Press Salts", "https://github.com/idleberg/sublime-wordpress-salts", True)
]

def plugin_loaded():
    from package_control import events

    if events.install(package_name):
        print('Installed %s' % events.install(package_name))

    if events.post_upgrade(package_name):
        print('Upgraded %s' % events.install(package_name))

class InstallPackagesCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        from subprocess import check_call

        # Do we need to install packages?
        installed_packages = 0

        for package, repository, managed in packages:
            package_exists = self.package_exists(package)
            if package_exists:
                installed_packages += 1

        if installed_packages is len(packages):
            sublime.message_dialog("All available packages have already been installed!")
            return

        # Let's install packages
        message = "Do you really want to install {} package(s)?".format(len(packages) - installed_packages)
        response = sublime.ok_cancel_dialog(message, "Install Packages")

        if response is True:
            # Get package folders
            packages_path, installed_packages_path = self.get_packages_paths()

            if len(packages) > 0:
                ignored_packages = self.load_settings().get("ignored_packages")

                for package, repository, managed in packages:

                    os.chdir(packages_path)

                    if package in ignored_packages:
                        message = str("Skipping '{}', package on ignore list".format(package))
                        self.puts(message)
                    elif not self.package_exists(package):
                        message = "Cloning '{}' to {}".format(package, packages_path)
                        self.puts(message)

                        # Run in separate thread
                        sublime.set_timeout_async(self.clone(package, repository), 0)

                        # Add to Package Control settings, so packages will be updated
                        if managed:
                            self.add_to_package_control(package)
                            self.write_metadata(package, repository, packages_path)
                    else:
                        message = str("Skipping '{}', package is already installed".format(package))
                        self.puts(message)
            else:
                sublime.status_message("No packages specified")

    def load_settings(self):
        return sublime.load_settings("floatwork.sublime-settings")

    def puts(self, message):
        print(message)
        sublime.status_message(message)

    def clone(self, package, repository):
        from subprocess import check_call

        check_call(["git", "clone", repository, package, "--depth=1"])

    def add_repository(self, repository):
        pc_settings = sublime.load_settings("Package Control.sublime-settings")

        repositories = pc_settings.get("repositories")

        if repository not in repositories:
            repositories.append(repository)
            repositories.sort()

            pc_settings.set("repositories", repositories)
            sublime.save_settings("Package Control.sublime-settings")

    def add_to_package_control(self, package):
        pc_settings = sublime.load_settings("Package Control.sublime-settings")

        installed_packages = pc_settings.get("installed_packages")

        if package not in installed_packages:
            installed_packages.append(package)
            installed_packages.sort()

            pc_settings.set("installed_packages", installed_packages)
            sublime.save_settings("Package Control.sublime-settings")

    def get_packages_paths(self):
        packages_path = sublime.packages_path()
        installed_packages_path = sublime.installed_packages_path()

        return packages_path, installed_packages_path

    def package_exists(self, package):
        packages_path, installed_packages_path = self.get_packages_paths()

        package_exists = os.path.isdir(os.path.join(packages_path, package))
        installed_package_exists = os.path.isfile(os.path.join(installed_packages_path, package + ".sublime-package"))

        return True if package_exists or installed_package_exists else False

    def write_metadata(self, package, repository, packages_path):
        from json import dump

        description = "Injected {} package".format(package)

        metadata = {
          "dependencies": [],
          "description": description,
          "platforms": [
            "*"
          ],
          "sublime_text": ">=3103",
          "url": repository,
          "version": "0.0.0"
        }

        filename = "{}/{}/package-metadata.json".format(packages_path, package)

        with open(filename, 'w') as outfile:
            dump(metadata, outfile)
